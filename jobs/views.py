from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from jobs.models import Job
from jobs.models import Cities, JobTypes

# 定义joblist作为视图
def joblist(request):
    # Job.objects.order_by是django model里面的语法
    # 可以从数据库读取职位列表，并按职位类型排序
    job_list = Job.objects.order_by('job_type')
    template = loader.get_template('joblist.html')
    context = {'job_list':job_list}
    
    for job in job_list:
        job.city_name = Cities[job.job_city][1]
        job.job_type = JobTypes[job.job_type][1]
        
    return HttpResponse(template.render(context))