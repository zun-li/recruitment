from django.urls import re_path
from jobs import views

urlpatterns = [
    # 职位列表
    re_path(r"^joblist/", views.joblist, name="joblist")
]