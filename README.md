# Django项目：招聘管理系统

## Quick Start

1. 将项目克隆到本地

   ~~~shell
   git clone https://gitlab.com/zun-li/recruitment.git
   ~~~

2. 安装conda，并创建创建`django`虚拟环境

   ~~~shell
   conda create -n django python=3.12
   ~~~

3. 进入`django`虚拟环境

   ~~~shell
   conda activate django
   ~~~

4. 安装必要的库

   ~~~shell
   conda install django
   ~~~

5. 进入项目文件夹，启动项目

   ~~~shell
   cd recruitment
   python manage.py runserver 0.0.0.0:8080
   ~~~

6. 访问项目

   ~~~shell
   http://127.0.0.1:8080
   ~~~

   

## Tutorial

### 1. 创建Admin后台账号

创建会议室管理项目，项目名为 `recruitment`

~~~shell
django-admin startproject recruitment
cd recruitment
~~~

启动项目

~~~shell
python manage.py runserver 0.0.0.0:8080
~~~

访问项目

~~~shell
http://127.0.0.1:8080
~~~

 运行项目时发现终端有一串警告，需要使用`makemigrations`创建数据库的一个迁移，产生`SQL`脚本

~~~shell
python manage.py makemigrations
~~~

然后使用`migrate`命令把默认的`Model`同步到数据库，`django`会自动在数据库为这些`model`建立相应的表，再次启动页面就没有产生错误

~~~shell
python manage.py migrate
~~~

创建一个管理员

~~~ shell
python manage.py createsuperuser
# 提示输出用户名、邮箱、密码
~~~

登录管理员页面

~~~shell
http://127.0.0.1:8000/admin
~~~

 项目的结构如下

~~~shell
recruitment
  ├── recuitment
    ├── __init__.py
    ├── asgi.py # asynchronous server gateway interface
    ├── settings.py # 整个Django项目的配置文件
    ├── urls.py # 整个Django项目的URL声明
    └── wsgi.py # web server gateway interface
  ├── db.sqlite3
  └── manage.py
~~~

~~~shell
# 可以修改 settings.py 中的一些配置

TIME_ZONE = 'Asia/Shanghai' # 时区配置成上海时区
LANGUAGE_CODE = 'zh-hans' # 语言改成中文
~~~

### 2. 创建职位管理应用

  项目中创建`jobs`应用

~~~shell
django-admin startapp jobs
~~~

项目的结构如下

~~~shell
recruitment
  ├── jobs
    ├── __init__.py
    ├── admin.py
    ├── apps.py
    ├── models.py # 在这里定义我们的脚本
    ├── tests.py
    └── views.py # 视图层
  ├── recuitment
    ├── __init__.py
    ├── asgi.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
  ├── db.sqlite3
  └── manage.py
~~~

将`jobs`添加到`settings`的`INSTALLED_APPS`列表中

在`models.py`中定义职位的模型

 ~~~python
 # models.py
 from django.db import models
 from django.contrib.auth.models import User
 
 # Create your models here.
 
 JobTypes = [
     (0, "技术类"),
     (1, "产品类"),
     (2, "运营类"),
     (3, "设计类")
 ]
 
 Cities = [
     (0, "北京"),
     (1, "上海"),
     (2, "深圳")
 ]
 
 class Job(models.Model):
     job_type = models.SmallIntegerField(blank=False, choices=JobTypes, verbose_name="职位类别")
     job_name = models.CharField(max_length=250, blank=False, verbose_name="职位名称")
     job_city = models.SmallIntegerField(choices=Cities, blank=False, verbose_name="工作地点")
     job_reponsibility = models.TextField(max_length=1024, verbose_name="职位职责")
     job_requirement = models.TextField(max_length=1024, blank=False, verbose_name="职位要求")
     creator = models.ForeignKey(User, verbose_name="创建人", null=True, on_delete=models.SET_NULL)
     created_date = models.DateTimeField(verbose_name="创建日期")
     modified_date = models.DateTimeField(verbose_name="修改时间")
 ~~~

 将`Model`中的`Job`应用注册到`admin`里面

~~~python
# admin.py
from django.contrib import admin
from jobs.models import Job

# Register your models here.

admin.site.register(Job) 
~~~

此时刷新`http://127.0.0.1:8000/admin`页面，`Jobs`应用出现，但点击进去报错，因为没有创建`Jobs`的表，数据库的表没有同步

~~~python
python manage.py makemigrations # 创建了数据库脚本
python manage.py migrate # 使数据库改动生效
~~~

此时进入`Jobs`界面，就可以添加新的职位了

<img src='readme.assets/01.png'>

### 3. 快速迭代完善应用

添加职位的时候，希望“创建日期”和“修改时间”有默认值

~~~python
# 在models.py的Job类中，将created_date modified_date传入默认值
from datetime import datetime

created_date = models.DateTimeField(verbose_name="创建日期", default=datetime.now)
modified_date = models.DateTimeField(verbose_name="修改时间", default=datetime.now)
~~~

在职位列表页，展现“职位的名称”、“职位的类型”等字段

~~~python
# 在admin.py中添加JobAdmin类，可以定义在页面上展示哪些字段

class JobAdmin(admin.ModelAdmin):
    # list_display是ModelAdmin中定义的有特定含义的属性
    list_display = ('job_name', 'job_type', 'job_city', 'creator', 'created_date', 'modified_date')
    

# 将JobAdmin注册到网站站点里面
admin.site.register(Job, JobAdmin)
~~~

现在能够在列表页里面查看到职位的有关信息，点职位名称就能进入修改页

<img src='readme.assets/02.png'>

修改页中希望将“创建人”、“创建日期”、“修改时间”都隐藏起来，自动创建

~~~python
# 在admin.py的JobAdmin类里定义exclude属性

class JobAdmin(admin.ModelAdmin):
    # 将“创建人”、“创建日期”、“修改日期”隐藏
    exclude = ('creator', 'created_date', 'modified_date')
    list_display = ('job_name', 'job_type', 'job_city', 'creator', 'created_date', 'modified_date')
    
    # 利用ModelAdmin的save_model方法，可以让模型保存之前，去做一些操作
    def save_model(self, request, obj, form, change):
        obj.creator = request.user
        # 调用父类的方法来保存对象
        super().save_model(request, obj, form, change)
~~~

此时，创建职位的页面里面，“创建人”、“创建日期”、“修改时间”被隐藏，会默认生成一个创建人

<img src='readme.assets/03.png'>

 <img src='readme.assets/04.png'>

### 4. 定制页面展示职位列表   

添加页面`job/templates/base.html`

添加页面`job/templates/joblist.html`（继承自`base.html`）

~~~html
<!-- base.html -->
<h1 style="margin:auto;width:50%;">Django科技开放职位</h1>
<p></p>
{% block content %}
{% endblock %}
~~~

~~~html
<!-- joblist.html -->
{% extends 'base.html' %}

{% block content %}

{% if job_list %}
    <ul>
    {% for job in job_list %}
        <li>{{job.type_name}}  <a href="/job/{{ job.id }}/" style="color:blue">{{ job.job_name }}</a>   {{job.city_name}}  </li>
    {% endfor %}
    </ul>
{% else %}
    <p>No jobs are available.</p>
{% endif %}

{% endblock %}
~~~

在`views.py`中把自定义的页面加进来

~~~python
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from jobs.models import Job
from jobs.models import Cities, JobTypes

# 定义joblist作为视图
def joblist(request):
    # Job.objects.order_by是django model里面的语法
    # 可以从数据库读取职位列表，并按职位类型排序
    job_list = Job.objects.order_by('job_type')
    template = loader.get_template('joblist.html')
    context = {'job_list':job_list}
    
    for job in job_list:
        job.city_name = Cities[job.job_city][1]
        job.job_type = JobTypes[job.job_type][1]
        
    return HttpResponse(template.render(context))
~~~

把`joblist`视图注册到`url`路径中

 ~~~python
 # jobs/urls.py
 from django.urls import re_path
 from jobs import views
 
 urlpatterns = [
     # 职位列表
     re_path(r"^joblist/", views.joblist, name="joblist")
 ]
 ~~~

~~~python
# recruitment/urls.py
from django.urls import include, re_path
from django.contrib import admin
from django.urls import path

urlpatterns = [
    re_path(r"^", include("jobs.urls")),
    path('admin/', admin.site.urls),
] 
~~~

